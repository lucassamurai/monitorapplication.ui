﻿using HealthChecks.UI.Client;
using HealthChecks.UI.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitorApplication.UI.HealthCheck
{
    public static class HealthCheckExtensions
    {
        private static ILogger _logger = Log.ForContext(typeof(HealthCheckExtensions));

        public static IHealthChecksBuilder AddHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            _logger.Information("Add health check");

            services.Configure<HealthCheckApps>(configuration.GetSection("HealthCheckApps"));

            var apps = configuration.GetSection("HealthCheckApps").Get<HealthCheckApps>();

            var builder = services.AddHealthChecks();

            if (apps == null)
            {
                _logger.Error("No applications configured for health check");
                throw new Exception("No applications configured for health check");
            }

            //Filter Urls for Tag

            //try
            //{
            //    _logger.Information("Add url groups in health check");

            //    apps.Homologation.ForEach(hml => builder.AddUrlGroup(new Uri(hml.Uri), name: hml.Name, tags: new string[] { "homolog" }));

            //    apps.Production.ForEach(prd => builder.AddUrlGroup(new Uri(prd.Uri), name: prd.Name, tags: new string[] { "prod" }));
            //}
            //catch (Exception ex)
            //{
            //    _logger.Error("Url groups in health check error: " + ex.Message);
            //    throw ex;
            //}

            services.AddHealthChecksUI((options) =>
            {
                apps.Production.OrderBy(x => x.Name).ToList().ForEach(prd => options.AddHealthCheckEndpoint(prd.Name, prd.Uri));
                //apps.Homologation.OrderBy(x => x.Name).ToList().ForEach(hml => options.AddHealthCheckEndpoint(hml.Name, hml.Uri));

                //options.SetApiMaxActiveRequests(3);
                //options.SetEvaluationTimeInSeconds(900);
                //options.SetMinimumSecondsBetweenFailureNotifications(1800);

                //options.AddWebhookNotification(name: "Slack - #monitor-applications",
                //               uri: "http://monitor.sellercenter.app.br//status-dashboard",
                //               payload: "{\"text\":\"*[[LIVENESS]]*: [[DESCRIPTIONS]]. <http://monitor.sellercenter.app.br//status-dashboard|Click here> to more details.\"}",
                //               restorePayload: "{\"text\":\"*[[LIVENESS]]* is back to life\"}",
                //               shouldNotifyFunc: report => DateTime.UtcNow.Hour >= 8 && DateTime.UtcNow.Hour <= 23,
                //                                customMessageFunc: (report) =>
                //                                {
                //                                    var failing = report.Entries.Where(e => e.Value.Status == UIHealthStatus.Unhealthy);
                //                                    return $"{failing.Count()} healthchecks are failing";
                //                                },
                //                                customDescriptionFunc: report =>
                //                                {
                //                                    var failing = report.Entries.Where(e => e.Value.Status == UIHealthStatus.Unhealthy);
                //                                    return $"HealthChecks with names {string.Join("/", failing.Select(f => f.Key))} are failing";
                //                                });
            }).AddInMemoryStorage();

            return builder;
        }

        public static IApplicationBuilder UseHealthCheck(this IApplicationBuilder app, IConfiguration configuration)
        {
            // Gera o endpoint que retornará os dados utilizados no dashboard

            //Filter Urls for Tag

            //app.UseHealthChecks("/status-api/prod", new HealthCheckOptions
            //{
            //    Predicate = check => check.Tags.Contains("prod"),
            //    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            //});

            //app.UseHealthChecks("/status-api/homolog", new HealthCheckOptions
            //{
            //    Predicate = check => check.Tags.Contains("homolog"),
            //    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            //});


            app.UseHealthChecks("/status-api", new HealthCheckOptions
            {
                Predicate = _ => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            // Ativa o dashboard para a visualização da situação de cada Health Check
            app.UseHealthChecksUI(opt =>
            {
                opt.UIPath = "/status-dashboard";
            });

            return app;
        }
    }
}
