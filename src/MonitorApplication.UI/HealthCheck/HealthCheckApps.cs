﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitorApplication.UI.HealthCheck
{
    public class HealthCheckApps
    {
        public List<UrlApp> Homologation { get; set; }
        public List<UrlApp> Production { get; set; }

        public HealthCheckApps()
        {
            Homologation = new List<UrlApp>();
            Production = new List<UrlApp>();
        }
    }

    public class UrlApp
    {
        public string Name { get; set; }
        public string Uri { get; set; }

        public UrlApp()
        {
            Name = string.Empty;
            Uri = string.Empty;
        }
    }
}
